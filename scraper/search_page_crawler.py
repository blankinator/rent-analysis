import datetime
import sys
import logging

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from lxml import etree

logger = logging.getLogger()
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel(logging.INFO)


def scrape_wohnungsboerse_listings(base_url: str, offset: int = 0, limit: int = None,
                                   driver: webdriver.Chrome = None) -> list:
    """
    Scrapes the listings of Wohnungsboerse and fetches data available on the list view for a given amount of pages
    :param base_url: base url to start of from
    :param offset: give, if scraping should not start at the beginning but at page x
    :param limit: give, if scraping should stop after x pages
    :return: list of dictionaries with the data of each listing
    """

    if driver is None:
        # configure webdriver
        logger.info("Initializing web driver")
        options = Options()
        options.headless = True  # hide GUI
        options.add_argument("--window-size=1920,1080")  # set window size to native GUI size
        options.add_argument("start-maximized")  # ensure window is full-screen

        driver = webdriver.Chrome(options=options)
    logger.info(f"Moving to base url {base_url}")
    driver.get(base_url)
    # wait for page to load
    WebDriverWait(driver=driver, timeout=5).until(
        EC.presence_of_element_located((By.LINK_TEXT, "alle Mietwohnungen in Leipzig"))
    )
    logger.info("Reached base url")

    def fetch_page(page_index: int = 0, data: list = None) -> list:

        # initialize data dict, if not done yet
        if not data:
            data = list()
            logger.info("Crawling first page, initializing data list...")

        # return data on completion, if limit is set
        if limit and page_index >= limit:
            logger.info(f"Limit reached at page index {page_index}. Returning data...")
            return data

        # goto all apartments view, if not done yet, else load next page
        if len(driver.find_elements(By.LINK_TEXT, "alle Mietwohnungen in Leipzig")) > 0 and page_index - offset == 0:
            logger.info("Moving to listing view...")
            driver.execute_script(f"AjaxSearch.showPage({1})")
            driver.implicitly_wait(0.5)
            WebDriverWait(driver=driver, timeout=5).until(
                EC.presence_of_element_located(
                    (By.XPATH,
                     f"//div[contains(@class, 'search_result_entry') and @itemprop='offers']"))
            )
            logger.info("Listing view reached")
        else:
            try:
                logger.info(f"Moving to listing view page {page_index + 1}")
                # btn_next_page = driver.find_element(
                #     By.XPATH,
                #     f"//span[contains(@class, 'nextpages')]//a[@onclick='AjaxSearch.showPage({page_index + 1})']")
                # btn_next_page.click()
                driver.execute_script(f"AjaxSearch.showPage({page_index + 1})")
                driver.implicitly_wait(0.5)
                # wait for page to load
                WebDriverWait(driver=driver, timeout=5).until(
                    EC.presence_of_element_located(
                        (By.XPATH,
                         f"//span[contains(@class, 'nextpages')]//a[@onclick='AjaxSearch.showPage({page_index + 2})']"))
                )
                logger.info(f"Reached listing view page {page_index}")
            except Exception as e:
                print(e)
                raise KeyError("page navigation link not found")

        # scrape links to listings
        # souped = BeautifulSoup(driver.page_source, 'html.parser')
        # elements = souped.find_all(class_="search_result_entry")
        html = etree.HTML(driver.page_source)
        elements = html.xpath("//div[contains(@class, 'search_result_entry') and @itemprop='offers']")

        def extract_data(element) -> dict:
            data_element = element.xpath('.//div[contains(@class, "search_result_entry-data")]')[0]
            listing_url = data_element.xpath(".//h3/a/@href")[0]
            listing_id = listing_url.split('/')[-1]
            thumbnail_url = element.xpath('.//img/@data-src')[0]
            thumbnail_alt = element.xpath('.//img/@alt')[0]
            headline = data_element.xpath(".//h3/a/@title")[0]
            district = data_element.xpath(".//div[contains(@class, 'search_result_entry-subheadline')]/text()")[
                0].strip()
            rent_cold = data_element.xpath(".//meta[@itemprop='price']/@content")[0]
            currency = data_element.xpath(".//meta[@itemprop='priceCurrency']/@content")[0]
            latitude = data_element.xpath(".//meta[@itemprop='latitude']/@content")[0]
            longitude = data_element.xpath(".//meta[@itemprop='longitude']/@content")[0]
            country = data_element.xpath(".//meta[@itemprop='addressCountry']/@content")[0]
            city = data_element.xpath(".//meta[@itemprop='addressLocality']/@content")[0]
            floor_size = data_element.xpath(".//div[@itemprop='floorSize']/meta[@itemprop='value']/@content")[0]
            floor_size_unit = data_element.xpath(".//div[@itemprop='floorSize']/meta[@itemprop='unitCode']/@content")[0]
            num_of_rooms = data_element.xpath(".//div[@itemprop='numberOfRooms']/meta[@itemprop='value']/@content")[0]
            try:
                extras = data_element.xpath(".//div[contains(@class, 'search_result_entry-furnishings')]//li/text()")
            except IndexError:
                extras = None
                pass

            data_dict = {
                "listing_id": str(listing_id),
                "listing_url": str(listing_url),
                "thumbnail_url": str(thumbnail_url),
                "thumbnail_alt": str(thumbnail_alt),
                "headline": str(headline),
                "district": str(district),
                "rent_cold": float(rent_cold),
                "currency": str(currency),
                "latitude": float(latitude),
                "longitude": float(longitude),
                "country": str(country),
                "city": str(city),
                "floor_size": float(floor_size),
                "floor_size_unit": str(floor_size_unit),
                "num_of_rooms": float(num_of_rooms),
                "extras": [str(extra) for extra in extras],
                "last_seen": datetime.datetime.now()
            }
            return data_dict

        # extract valuable data from DOM
        logger.info(f"Extracting data from listing view page {page_index}")
        extracted_data = [extract_data(element) for element in elements]

        # append data to list
        new_data = data + extracted_data

        # return if there are no more pages to scrape
        if len(
                driver.find_elements(
                    By.XPATH,
                    f"//span[contains(@class, 'nextpages')]//a[@onclick='AjaxSearch.showPage({page_index + 2})']")) == 0:
            logger.info("Reached end of listing view pages")
            return new_data

        return fetch_page(page_index + 1, new_data)

    # fetch and return scraped data
    return fetch_page()


def scrape_wohnungsboerse_details(listing_id: str, driver: webdriver.Chrome = None) -> dict:
    """
    Scrapes data of a single listing from its detail page
    :param listing_id: id of the listing to fetch data of
    :return:
    """

    url = f"https://www.wohnungsboerse.net/immodetail/{listing_id}"

    # configure webdriver
    if driver is None:
        logger.info("Initializing web driver")
        options = Options()
        options.headless = True  # hide GUI
        options.add_argument("--window-size=1920,1080")  # set window size to native GUI size
        options.add_argument("start-maximized")  # ensure window is full-screen
        driver = webdriver.Chrome(options=options)

    logger.info(f"Moving to url {url}")
    driver.get(url)
    # wait for page to load
    WebDriverWait(driver=driver, timeout=5).until(
        EC.presence_of_element_located((By.XPATH, "//div[@class='row data-table mt-3 mt-lg-4']/div[@class='col-6']"))
    )
    logger.info("Reached base url")

    def convert_intext_float(input):
        return float(input.split('\xa0')[0].replace(",", ".")) if input is not None else None

    # extract the data points that have not yet been scraped from the list view
    html = etree.HTML(driver.page_source)
    clicks = int(
        html.xpath("//div[@class='col-12 click-count-desktop d-none d-lg-block']/span/text()")[0].split(" ")[0])

    # a little more complex logic for extraction of additional attributes, since they do not follow a strict rule
    def get_index_or_none(list, value):
        try:
            return list.index(value)
        except ValueError:
            return None

    def get_labeled_value(labels, values, label):
        value_index = get_index_or_none(labels, label)
        try:
            return values[value_index]
        except (IndexError, TypeError):
            return None

    main_data_raw = [x.strip() for x in html.xpath(
        "//div[@class='col-12 col-lg-8 text-left  p-3 p-lg-0 pt-3 pt-lg-40']/div[@class='row data-table mt-3']/*/text()")]
    main_data_labels = [main_data_raw[i] for i in range(len(main_data_raw)) if i % 2 == 0]

    main_data = [main_data_raw[i] for i in range(len(main_data_raw)) if i % 2 == 1]

    apartment_type = get_labeled_value(main_data_labels, main_data, "Objekttyp:")
    available_from_raw = get_labeled_value(main_data_labels, main_data, "Frei ab:")
    available_from = None if available_from_raw == "keine Angabe" else available_from_raw
    floor_raw = get_labeled_value(main_data_labels, main_data, "Etage:")
    floor = floor_raw.replace("\n", "") if floor_raw else None

    price_data_raw = [x.strip() for x in html.xpath(
        "//div[@class='col-12 col-lg-8 text-left  p-3 p-lg-0']/div[@class='row data-table mt-3']/*/text()")]
    price_data_labels = [price_data_raw[i] for i in range(len(price_data_raw)) if i % 2 == 0]

    price_data = [price_data_raw[i] for i in range(len(price_data_raw)) if i % 2 == 1]

    service_charges = convert_intext_float(get_labeled_value(price_data_labels, price_data, "Nebenkosten:"))
    try:
        deposit = convert_intext_float(get_labeled_value(price_data_labels, price_data, "Kaution:"))
    except ValueError:
        deposit = 0.0
    try:
        provision = convert_intext_float(get_labeled_value(price_data_labels, price_data, "Provision:"))
    except:
        provision = 0.0

    try:
        transfer_fee = convert_intext_float(get_labeled_value(price_data_labels, price_data, "Ablöse:"))
    except:
        transfer_fee = 0.0

    additional_data_labels = [x.strip() for x in html.xpath(
        "//div[@class='row data-table mt-3 mt-lg-4']/div[@class='col-6 thead']/text()")]
    additional_data = [x.strip() for x in
                       html.xpath("//div[@class='row data-table mt-3 mt-lg-4']/div[@class='col-6']/text()")]

    construction_year = get_labeled_value(additional_data_labels, additional_data, "Baujahr:")
    quality_of_facilities = get_labeled_value(additional_data_labels, additional_data, "Qualität der Ausstattung:")
    quality_of_property = get_labeled_value(additional_data_labels, additional_data, "Objektzustand:")
    heating_type = get_labeled_value(additional_data_labels, additional_data, "Befeuerungsart:")
    energy_certificate = get_labeled_value(additional_data_labels, additional_data, "Energieausweis:")
    energy_consumption_raw = get_labeled_value(additional_data_labels, additional_data, "Endenergiebedarf:")
    energy_consumption = energy_consumption_raw.split(' ')[0].replace(',',
                                                                      ".") if energy_consumption_raw is not None else None
    energy_efficiency_class = get_labeled_value(additional_data_labels, additional_data, "Energieeffizienzklasse:")

    street = html.xpath("//div[@class='col-12 col-lg-8 text-left  p-3 p-lg-0 mt-0 mt-lg-4']/text()")[1].strip()
    zip_code = html.xpath(
        "//div[@class='col-12 col-lg-8 text-left  p-3 p-lg-0 mt-0 mt-lg-4']/text()")[2].strip().split(' ')[0]

    try:
        user_type = html.xpath("//div[@class='user-type mb-3']/text()")[0].replace("\n", "").strip()
        immoscout_url = None
    except IndexError:
        if len(html.xpath("//div[@class='text-right mt-3 mt-lg-4']/div/a")) == 1:
            user_type = "ImmoScout"
            immoscout_url = html.xpath("//div[@class='text-right mt-3 mt-lg-4']/div/a")[0]
        else:
            user_type = None
            immoscout_url = None

    data = {
        "clicks": str(clicks) if clicks else None,
        "apartment_type": str(apartment_type) if apartment_type else None,
        "floor": str(floor) if floor else None,
        "available_from": str(available_from) if available_from else None,
        "service_charges": float(service_charges) if service_charges else None,
        "deposit": float(deposit) if deposit else None,
        "provision": provision or None,
        "transfer_fee": transfer_fee or None,
        "heating_type": str(heating_type) if heating_type else None,
        "energy_efficiency_class": str(energy_efficiency_class) if energy_efficiency_class else None,
        "energy_consumption": float(energy_consumption) if energy_consumption else None,
        "energy_certificate": str(energy_certificate) if energy_certificate else None,
        "construction_year": int(construction_year) if construction_year else None,
        "quality_of_facilities": str(quality_of_facilities) if quality_of_facilities else None,
        "quality_of_property": str(quality_of_property) if quality_of_property else None,
        "street": str(street) if street else None,
        "zip_code": str(zip_code) if zip_code else None,
        "user_type": str(user_type) if user_type else None,
        "immoscout_url": immoscout_url if immoscout_url else None
    }

    return data
