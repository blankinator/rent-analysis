from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import database_connection
from search_page_crawler import scrape_wohnungsboerse_listings, scrape_wohnungsboerse_details

immo_url = "https://www.wohnungsboerse.net/Leipzig/mieten/wohnungen"


def main() -> None:
    # initialize a web driver
    options = Options()
    options.headless = True  # hide GUI
    options.add_argument("--window-size=1920,1080")  # set window size to native GUI size
    options.add_argument("start-maximized")  # ensure window is full-screen
    driver = webdriver.Chrome(options=options)

    data = scrape_wohnungsboerse_listings(immo_url, limit=30, driver=driver)

    new_data = [listing | scrape_wohnungsboerse_details(listing["listing_id"], driver) for listing in data]

    database_connection.add_data(new_data)


if __name__ == "__main__":
    main()
