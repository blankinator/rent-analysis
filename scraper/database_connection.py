from pymongo import MongoClient

database_url = "65.21.140.171"
database_name = "wohnungsboerse"
database_user = "root"
database_password = "ag_link"


def get_database():
    # Provide the mongodb atlas url to connect python to mongodb using pymongo
    CONNECTION_STRING = f"mongodb://{database_user}:{database_password}@{database_url}"

    # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
    client = MongoClient(CONNECTION_STRING)

    return client


def add_data(data: list) -> None:
    client = get_database()
    db = client[database_name]
    collection = db['listings']

    def insert_item(item):
        try:
            collection.update_one({"listing_id": item["listing_id"]}, {"$set": item}, upsert=True)
        except Exception as e:
            print(e, item)
            pass

    [insert_item(item) for item in data]

    client.close()
